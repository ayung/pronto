'use strict';

describe('Service: conection', function () {

  // load the service's module
  beforeEach(module('nuevoApp'));

  // instantiate service
  var conection;
  beforeEach(inject(function (_conection_) {
    conection = _conection_;
  }));

  it('should do something', function () {
    expect(!!conection).toBe(true);
  });

});
