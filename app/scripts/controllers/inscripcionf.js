'use strict';

/**
 * @ngdoc function
 * @name nuevoApp.controller:InscripcionfCtrl
 * @description
 * # InscripcionfCtrl
 * Controller of the nuevoApp
 */
angular.module('nuevoApp')
  .controller('InscripcionfCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
