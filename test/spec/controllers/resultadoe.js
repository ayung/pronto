'use strict';

describe('Controller: ResultadoeCtrl', function () {

  // load the controller's module
  beforeEach(module('nuevoApp'));

  var ResultadoeCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ResultadoeCtrl = $controller('ResultadoeCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ResultadoeCtrl.awesomeThings.length).toBe(3);
  });
});
