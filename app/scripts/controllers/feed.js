'use strict';

/**
 * @ngdoc function
 * @name nuevoApp.controller:FeedCtrl
 * @description
 * # FeedCtrl
 * Controller of the nuevoApp
 */
angular.module('nuevoApp')
  .controller('FeedCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
