'use strict';

/**
 * @ngdoc function
 * @name nuevoApp.controller:InscripcioneCtrl
 * @description
 * # InscripcioneCtrl
 * Controller of the nuevoApp
 */
angular.module('nuevoApp')
  .controller('InscripcioneCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
