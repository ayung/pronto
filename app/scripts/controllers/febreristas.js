'use strict';

/**
 * @ngdoc function
 * @name nuevoApp.controller:FebreristasCtrl
 * @description
 * # FebreristasCtrl
 * Controller of the nuevoApp
 */
angular.module('nuevoApp')
  .controller('FebreristasCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
