'use strict';

/**
 * @ngdoc function
 * @name nuevoApp.controller:ResultadofCtrl
 * @description
 * # ResultadofCtrl
 * Controller of the nuevoApp
 */
angular.module('nuevoApp')
  .controller('ResultadofCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
