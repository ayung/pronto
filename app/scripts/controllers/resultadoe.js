'use strict';

/**
 * @ngdoc function
 * @name nuevoApp.controller:ResultadoeCtrl
 * @description
 * # ResultadoeCtrl
 * Controller of the nuevoApp
 */
angular.module('nuevoApp')
  .controller('ResultadoeCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
