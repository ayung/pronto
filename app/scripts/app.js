'use strict';

/**
 * @ngdoc overview
 * @name nuevoApp
 * @description
 * # nuevoApp
 *
 * Main module of the application.
 */
angular
  .module('nuevoApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'LocalStorageModule',
    'ngRoute',
    'oitozero.ngSweetAlert',
    'ngSanitize',
    'ngTouch'
  ])

.config(['localStorageServiceProvider', function(localStorageServiceProvider){

    localStorageServiceProvider.setPrefix('ls');
  }])


  .config(function ($routeProvider) {
    $routeProvider

      .when('/home', {
        templateUrl: 'views/home.html',
        controller: 'HomeCtrl',
        controllerAs: 'home'
      })

      .when('/eneristas', {
        templateUrl: 'views/eneristas.html',
        controller: 'EneristasCtrl',
        controllerAs: 'eneristas'
      })
      .when('/febreristas', {
        templateUrl: 'views/febreristas.html',
        controller: 'FebreristasCtrl',
        controllerAs: 'febreristas'
      })
      .when('/resultadoe', {
        templateUrl: 'views/resultadoe.html',
        controller: 'ResultadoeCtrl',
        controllerAs: 'resultadoe'
      })
      .when('/resultadof', {
        templateUrl: 'views/resultadof.html',
        controller: 'ResultadofCtrl',
        controllerAs: 'resultadof'
      })
      .when('/feed', {
        templateUrl: 'views/feed.html',
        controller: 'FeedCtrl',
        controllerAs: 'feed'
      })
      .when('/inscripcione', {
        templateUrl: 'views/inscripcione.html',
        controller: 'InscripcioneCtrl',
        controllerAs: 'inscripcione'
      })
      .when('/inscripcionf', {
        templateUrl: 'views/inscripcionf.html',
        controller: 'InscripcionfCtrl',
        controllerAs: 'inscripcionf'
      })
      .otherwise({
        redirectTo: '/home'
      });
  });
