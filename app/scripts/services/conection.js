'use strict';

/**
 * @ngdoc service
 * @name nuevoApp.conection
 * @description
 * # conection
 * Service in the nuevoApp.
 */
angular.module('nuevoApp')

  .service('conection', function ($resource, $httpParamSerializerJQLike) {
    var servicio = $resource('http://www.saltala.cl/igniter/HernannMovil/:action', null,
          {
    'OneDate': { method:'POST',
                        params: { action : 'GetNumeroAtencion' },
                        headers : {"Content-Type": "application/x-www-form-urlencoded"},
                        transformRequest: function(data) {
                        return $httpParamSerializerJQLike(data);
                  }},
      });

    return servicio;
  })

  .service('ConectionUser', function($resource, $httpParamSerializerJQLike) {
    var user = $resource('http://saltala.cl/igniter/Apptotem/:action', null,
      {
    'GetCredentials': { method:'POST',
                        params: { action : 'GetUsuariosAdminSucursal' },
                        headers : {"Content-Type": "application/x-www-form-urlencoded"},
                        transformRequest: function(data) {
                        return $httpParamSerializerJQLike(data);
                  }},

      });

    return user;
  })


  .service('Almacen', function() {

    return {
        setData: function(nuevaData) {
          localStorage.cerro = JSON.stringify(nuevaData);
        },
        getData: function() {
          var data = null;
          try{
              data = JSON.parse(localStorage.cerro);
          } catch(e){
              data = null;
          }
          return data;
        },
        eraseData: function() {
          localStorage.cerro = null;
        }
    };
  });
