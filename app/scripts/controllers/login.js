'use strict';

/**
 * @ngdoc function
 * @name nuevoApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the nuevoApp
 */
angular.module('nuevoApp')

  .controller('LoginCtrl', function ($scope,$location,Almacen,ConectionUser) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ]; 
  
  
      $scope.send = function(){


      ConectionUser.GetCredentials({

      username: $scope.logeo.username,
      pass_user: $scope.logeo.pass_user

    }, function(response) {

      if(response.success === "true"){

        $scope.modulo = response.data;
        console.log(JSON.stringify(response.data));

        Almacen.setData(response);

         SweetAlert.swal({
         title: "Bienvenido!",
         text: "Este es el sistema de turnos de atención.",
         imageUrl: "images/fila.png" });

         $location.path('/main');

  
      }

      else if ($scope.logeo.username === null && $scope.logeo.pass_user === null) {

     SweetAlert.warning("No existen datos!", "Ingrese usuario y contraseña!");

     }

      else {

       SweetAlert.error("Datos incorrectos!", "Verifique Usuario o contraseña!");
      }

});
};
  
  });
